import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
    kotlin("plugin.serialization") version "1.6.21"
}

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(compose.desktop.currentOs)
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.21")
}

compose.desktop {
    application {
        mainClass = "com.daelibs.kiosk.setup.ui.MainScreenKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Exe)
            packageName = "DaelibsSetupTool"
            packageVersion = "1.0.8"
            description = "Quick Setup Tool for Daelibs Devices."
            args += listOf("-v=${packageVersion}")

            windows {
                upgradeUuid = "5c7c6abb-637b-41fe-8bc2-f507492b6bda"
            }
        }
    }
}
