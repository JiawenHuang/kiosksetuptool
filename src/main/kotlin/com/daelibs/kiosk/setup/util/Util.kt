package com.daelibs.kiosk.setup.util

import kotlin.math.min

object Util {
    fun millis2FitTimeSpan(millis: Long, precision: Int = 4): String {
        var tmpMillis = millis
        var tmpPrecision = precision
        if (tmpPrecision <= 0) return ""
        tmpPrecision = min(tmpPrecision, 5)
        val units = arrayOf("d", "h", "m", "s", "ms")
        if (tmpMillis == 0L) return 0.toString() + units[tmpPrecision - 1]
        val sb = StringBuilder()
        if (tmpMillis < 0) {
            sb.append("-")
            tmpMillis = -tmpMillis
        }
        val unitLen = intArrayOf(86400000, 3600000, 60000, 1000, 1)
        for (i in 0 until tmpPrecision) {
            if (tmpMillis >= unitLen[i]) {
                val mode = tmpMillis / unitLen[i]
                tmpMillis -= mode * unitLen[i]
                sb.append(mode).append(units[i])
            }
        }
        return sb.toString()
    }
}