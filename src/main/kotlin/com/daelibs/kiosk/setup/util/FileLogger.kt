package com.daelibs.kiosk.setup.util

import com.daelibs.kiosk.setup.model.Config
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

object FileLogger {

    private val logFileWriter = FileWriter(File(Config.logFolderPath + "kst.log").apply {
        if (!exists()) {
            parentFile.mkdirs()
        }
    }, true)
    private val logFileWriterMap = mutableMapOf<String, FileWriter>()
    val logTimestampFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ", Locale("en", "AU"))

    fun log(kioskId: String? = null, text: String) {
        val nowMills = System.currentTimeMillis()
        val logText = buildString {
            append(logTimestampFormat.format(nowMills))
            append("Kiosk: ${kioskId ?: "N/A"} -> ")
            append(text)
        }
        println(logText)
        val currentLogFileWriter = if (kioskId != null && kioskId != "N/A") {
            if (logFileWriterMap[kioskId] == null) {
                logFileWriterMap[kioskId] = FileWriter(
                    Config.logFolderPath + "kst_${kioskId.split("/").joinToString(separator = "_")}.log",
                    true
                )
            }
            logFileWriterMap[kioskId]!!
        } else {
            logFileWriter
        }
        currentLogFileWriter.append(logText).appendLine()
        currentLogFileWriter.flush()
    }
}