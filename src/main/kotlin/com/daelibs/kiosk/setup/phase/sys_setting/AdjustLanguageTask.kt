package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class AdjustLanguageTask(taskIndex: Int) : CmdTask(taskIndex, "Adjust Language") {
    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                when (model) {
                    AndroidDeviceModel.P90 -> {
                        openInputAndEnterSystemSettings("System")
                        navDown()
                        navClick()
                        navClick()
                    }

                    AndroidDeviceModel.KX20 -> {
                        openInputAndEnterSystemSettings("language")
                        navClick()
                    }
                }
                //check if language set
                var langUINodes = dumpUI()
                val auLangText = "English (Australia)"
                var isOnlyAuLang = langUINodes.count { it.resId == "com.android.settings:id/label" } == 1
                val isLangSet = langUINodes.firstOrNull { it.index == "0" && it.text == auLangText } != null &&
                        isOnlyAuLang
                onTaskLog("Check English (Australia) if primary: $isLangSet.")
                if (!isLangSet) {
                    val isAuLangAdded = langUINodes.firstOrNull { it.text == auLangText } != null
                    if (!isAuLangAdded) {
                        langUINodes.first { it.text == "Add a language" }.let {
                            //click add a language button
                            click(it.getCentrePoint())
                            //click right top search button
                            click(1890, 103)
                            input("english")
                            navBack()
                            navDown()
                            navClick()
                            dumpUI().first {
                                it.text.contains("Australia")
                            }.let {
                                click(it.getCentrePoint())
                                onTaskLog("Add English (Australia) into list.")
                                langUINodes = dumpUI()
                            }
                        }
                    }
                    isOnlyAuLang = langUINodes.count { it.resId == "com.android.settings:id/label" } == 1
                    if (!isOnlyAuLang) {
                        langUINodes.first { it.contentDesc == "More options" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.text == "Remove" }.run { click(getCentrePoint()) }
                        val langRemoveScreen = dumpUI()
                        langRemoveScreen.filter { it.resId == "com.android.settings:id/checkbox" && it.text != auLangText }
                            .forEach {
                                click(it.getCentrePoint())
                                onTaskLog("Select ${it.text}.")
                            }
                        langRemoveScreen.first { it.contentDesc == "Remove" }.run { click(getCentrePoint()) }
                        dumpUI().firstOrNull { it.text == "OK" || it.text == "REMOVE" }?.run {
                            click(getCentrePoint())
                            onTaskLog("Remove selected languages.")
                        }
                    }
                }
            }
        }
    }
}

