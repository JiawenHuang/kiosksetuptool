package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class CheckDoNotDisturbTask(taskIndex: Int) : CmdTask(taskIndex, "Check Do not disturb mode") {
    companion object {
        private val dndModeMap =
            mapOf("0" to "off", "1" to "priority only", "2" to "total silence", "3" to "alarms only")
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                val dndMode = runCommandSync(
                    getId(),
                    concatCmd("shell settings get global zen_mode")
                )
                onTaskLog("Current do not disturb mode is ${dndModeMap[dndMode]}.")
                if (dndMode != "2") {
                    when(model) {
                        AndroidDeviceModel.P90 -> {
                            runCommandDelay(getId(), concatCmd("shell cmd statusbar collapse"))
                            runCommandDelay(getId(), concatCmd("shell cmd statusbar expand-settings"))
                            dumpUI().first { it.contentDesc.contains("Do Not Disturb") }
                                .run {
                                    if (text.equals("Off", true)) {
                                        click(getCentrePoint())
                                    }
                                }
                            runCommandDelay(getId(), concatCmd("shell cmd statusbar collapse"))
                            onTaskLog("Switch do not disturb mode on.")
                        }
                        AndroidDeviceModel.KX20 -> {
                            runCommandDelay(getId(), concatCmd("shell service call statusbar 2"))
                            runCommandDelay(getId(), concatCmd("shell service call statusbar 1"))
                            dumpUI().first { it.contentDesc == "Open quick settings." }.run { click(getCentrePoint()) }
                            dumpUI().first { it.contentDesc == "Open Do not disturb settings." }
                                .run { click(getCentrePoint()) }
                            var dndScreen = dumpUI()
                            dumpUI().first { it.resId == "android:id/toggle" }.run {
                                if (!isChecked()) {
                                    click(getCentrePoint())
                                    dndScreen = dumpUI()
                                }
                            }
                            dndScreen.first { it.text.replace("\n", "").replace("\r", "") == "Totalsilence" }
                                .run { click(getCentrePoint()) }
                            dndScreen.first { it.text == "DONE" }.run { click(getCentrePoint()) }
                            runCommandSync(getId(), concatCmd("shell service call statusbar 2"))
                            onTaskLog("Switch current mode into total silence mode.")
                        }
                    }
                }
            }
        }
    }
}