package com.daelibs.kiosk.setup.phase

import com.daelibs.kiosk.setup.model.AndroidDevice
import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.service.AdbCmdService
import org.jetbrains.skiko.hostOs

class GetConnectedKiosks {
    companion object {
        private val PATTERN_CONNECTED_DEVICE =
            "(\\w+)\\s*device ${if (hostOs.isWindows) "" else "usb:(\\w+)\\s"}product:(\\w+)\\smodel:(\\w+)\\sdevice:(\\w+)\\stransport_id:(\\d+)".toRegex()
    }

    suspend fun execute(cmdService: AdbCmdService) =
        cmdService.runCommandSync(null, "adb devices -l").let { cmdRes ->
            PATTERN_CONNECTED_DEVICE.findAll(cmdRes).map {
                if (hostOs.isWindows) {
                    AndroidDevice(
                        it.groupValues[1],
                        it.groupValues[2],
                        AndroidDeviceModel.valueOf(it.groupValues[3]),
                        it.groupValues[4],
                        it.groupValues[5],
                    )
                } else {
                    AndroidDevice(
                        it.groupValues[1],
                        it.groupValues[3],
                        AndroidDeviceModel.valueOf(it.groupValues[4]),
                        it.groupValues[5],
                        it.groupValues[6]
                    )
                }
            }.toList()
        }
}