package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class RemovePreinstalledAppsTask(taskIndex: Int) : CmdTask(taskIndex, "Uninstall pre-installed apps") {

    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                if (model == AndroidDeviceModel.P90) {
                    onTaskLog("Skip this task.")
                    return
                }
                runCommandSync(getId(), concatCmd("remount"))
                var hasAppsRemoved =
                    runCommandSync(getId(), concatCmd("shell ls /vendor/operator")).isBlank()
                if (hasAppsRemoved) {
                    onTaskLog("Pre-installed apps has already been removed.")
                } else {
                    hasAppsRemoved = runCommandSync(
                        getId(), concatCmd("shell rm -rf /vendor/operator/app/")
                    ).isBlank()
                    if (hasAppsRemoved) {
                        onTaskLog("Remove pre-install apps ${parseResult(true)}.")
                    } else {
                        throw TaskFailedToExecException(getId(), "remove pre-install apps failed!")
                    }
                }
                val preInstalledGoogleApps = mapOf(
                    "Google Drive" to "/system/app/Drive/Drive.apk",
                    "Youtube" to "/system/app/YouTube/YouTube.apk",
                    "Play Store" to "/system/priv-app/Phonesky/Phonesky.apk",
                    "Play Music" to "/system/app/Music2/Music2.apk",
                    "Play Movies & TV" to "/system/app/GoogleVideos/GoogleVideos.apk"
                )
                preInstalledGoogleApps.forEach { (appName, appPath) ->
                    var isAppRemoved =
                        runCommandSync(getId(), concatCmd("shell ls $appPath")).contains("No such file or directory")
                    if (isAppRemoved) {
                        onTaskLog("$appName has already been removed.")
                    } else {
                        isAppRemoved = runCommandSync(
                            getId(), concatCmd("shell rm -f $appPath")
                        ).isBlank()
                        if (isAppRemoved) {
                            onTaskLog("Remove $appName ${parseResult(true)}.")
                        } else {
                            throw TaskFailedToExecException(getId(), "remove $appName failed!")
                        }
                    }
                }
            }
        }
    }
}