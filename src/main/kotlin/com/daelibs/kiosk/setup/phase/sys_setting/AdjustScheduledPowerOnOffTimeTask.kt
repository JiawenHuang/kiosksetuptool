package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.UINode
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class AdjustScheduledPowerOnOffTimeTask(taskIndex: Int) : CmdTask(taskIndex, "Adjust Scheduled PowerOnOffTime") {
    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                if (model == AndroidDeviceModel.P90) {
                    onTaskLog("Skip this task")
                    return
                }
                openInputAndEnterSystemSettings("schedule")
                val schedulePowerScreen = dumpUI()
                val scheduleTimeNodes =
                    schedulePowerScreen.filter { it.resId == "com.mediatek.schpwronoff:id/timeDisplay" }
                val scheduleDaysOfWeekNodes =
                    schedulePowerScreen.filter { it.resId == "com.mediatek.schpwronoff:id/daysOfWeek" }
                (0..1).forEach { index ->
                    val targetTime = if (index == 0) config.onTime else config.offTime
                    val isTimeSet = scheduleTimeNodes[index].text == targetTime
                    val isDaysOfWeekSet = scheduleDaysOfWeekNodes[index].text == "Every day"
                    adjustScheduleTimeAndDays(
                        cmdService,
                        deviceState,
                        scheduleTimeNodes[index],
                        isTimeSet,
                        isDaysOfWeekSet,
                        targetTime
                    )
                    onTaskLog("Schedule power ${if (index == 0) "on" else "off"} time at $targetTime every day.")
                }
                dumpUI().filter { it.resId == "com.mediatek.schpwronoff:id/alarmButton" && it.text == "ON" }
                    .forEach {
                        click(it.getCentrePoint())
                    }
            }
        }
    }

    private suspend fun adjustScheduleTimeAndDays(
        adbService: AdbCmdService,
        deviceItem: AndroidDeviceUiState,
        timeNode: UINode,
        isTimeSet: Boolean,
        isDaysOfWeekSet: Boolean,
        targetTime: String,
    ) {
        adbService.run {
            deviceItem.run {
                if (isTimeSet && isDaysOfWeekSet) {
                    return
                }
                click(timeNode.getCentrePoint())
                val scheduleOnScreen = dumpUI()
                if (!isTimeSet) {
                    if (targetTime.contains("am", true) || targetTime.contains("pm", true)) {
                        throw TaskFailedToExecException(
                            getId(),
                            "schedule power time format error, please use 24-hour format!"
                        )
                    }
                    val timeParts = targetTime.trim().split(":")
                    scheduleOnScreen.first { it.text == "Time" }.run { click(getCentrePoint()) }
                    dumpUI().first { it.resId == "android:id/toggle_mode" }.run { click(getCentrePoint()) }
                    (0..1).forEach { index ->
                        repeat(2) {
                            del()
                        }
                        repeat(2) {
                            forwardDel()
                        }
                        input(timeParts[index])
                        tab()
                    }
                    dumpUI().first { it.text == "OK" }.run { click(getCentrePoint()) }
                }
                if (!isDaysOfWeekSet) {
                    scheduleOnScreen.first { it.text == "Repeat" }.run { click(getCentrePoint()) }
                    val daysListScreen = dumpUI()
                    daysListScreen.filter { it.resId == "android:id/text1" && !it.isChecked() }.forEach {
                        click(it.getCentrePoint())
                    }
                    daysListScreen.first { it.text == "OK" }.run { click(getCentrePoint()) }
                }
                scheduleOnScreen.first { it.text == "DONE" }.run { click(getCentrePoint()) }
            }
        }
    }
}