package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class CheckPriorityOnlyAllowsTask(taskIndex: Int) : CmdTask(taskIndex, "Check Priority only allows") {
    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                when (model) {
                    AndroidDeviceModel.P90 -> {
                        openInputAndEnterSystemSettings("Calls%sthat%scan%sinterrupt")
                        swipe(1000, 824, 1000, 500)
                        dumpUI().run {
                            last { it.resId == "android:id/checkbox" }.run {
                                click(getCentrePoint())
                            }
                            last { it.resId == "android:id/switch_widget" }.run {
                                if (isChecked()) {
                                    click(getCentrePoint())
                                }
                            }
                            onTaskLog("Switch people to none")
                        }
                        openInputAndEnterSystemSettings("Alarms")
                        dumpUI().filter { it.resId == "android:id/switch_widget" && it.isChecked() }.forEach {
                            click(it.getCentrePoint())
                        }
                        onTaskLog("Switch alarms and other interruptions to none")
                    }

                    AndroidDeviceModel.KX20 -> {
                        openInputAndEnterSystemSettings("priority")
                        navClick()
                        val priorityOnlyAllowScreen = dumpUI()
                        priorityOnlyAllowScreen.filter { it.resId == "android:id/switch_widget" }
                            .forEachIndexed { index, uiNode ->
                                if (uiNode.isChecked()) {
                                    click(uiNode.getCentrePoint())
                                    val switchName = when (index) {
                                        0 -> "Alarms"
                                        1 -> "Reminders"
                                        2 -> "Messages"
                                        else -> "Repeat callers"
                                    }
                                    onTaskLog("Switch $switchName to off.")
                                }
                            }
                        val noneNodes = priorityOnlyAllowScreen.filter { it.resId == "android:id/summary" }.take(2)
                        val isMsgSetToNone = noneNodes[0].text == "None"
                        if (!isMsgSetToNone) {
                            click(noneNodes[0].getCentrePoint())
                            dumpUI().first { it.text == "None" }.run {
                                click(getCentrePoint())
                                onTaskLog("Switch Messages to None")
                            }
                        }
                        val isCallsSetToNone = noneNodes[1].text == "None"
                        if (!isCallsSetToNone) {
                            click(noneNodes[1].getCentrePoint())
                            dumpUI().first { it.text == "None" }.run {
                                click(getCentrePoint())
                                onTaskLog("Switch Calls to None")
                            }
                        }
                        dumpUI().last { it.resId == "android:id/switch_widget" }.run {
                            if (isChecked()) {
                                click(getCentrePoint())
                                onTaskLog("Switch $text to off")
                            }
                        }
                    }
                }
            }
        }
    }
}