package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.phase.SetupPhase
import kotlin.reflect.KClass

class PostSettingPhase : SetupPhase("Post Settings") {

    override val allTaskClasses: List<KClass<out CmdTask>>
        get() = listOf(
            MiscConfigTask::class,
            InstallAppsTask::class,
            DisableBatteryOptimisationTask::class,
            AllowAppsToInstallUpdatesTask::class,
            GrantDeviceOwnerTask::class,
            RemovePreinstalledAppsTask::class,
            RebootTask::class
        )
}