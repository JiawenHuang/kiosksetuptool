package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class SetWallpaperTask(taskIndex: Int) : CmdTask(taskIndex, "Set wallpaper") {
    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                when (model) {
                    AndroidDeviceModel.P90 -> {
                        openInputAndEnterSystemSettings("Wallpaper")
                        repeat(2) {
                            navDown()
                        }
                        navClick()
                        dumpUI().first { it.resId == "com.android.wallpaper:id/tile" }.run { click(getCentrePoint()) }
                        dumpUI().firstOrNull { it.resId == "com.android.permissioncontroller:id/permission_allow_button" }
                            ?.run {
                                click(getCentrePoint())
                            }
                        onTaskLog("Pick Portrait 800x1200.jpg as wallpaper.")
                        repeat(2) {
                            click(1000, 552)
                        }
                        dumpUI().first { it.resId == "com.android.wallpaper:id/preview_attribution_pane_set_wallpaper_button" }.run {
                            click(getCentrePoint())
                        }
                        dumpUI().first { it.resId == "com.android.wallpaper:id/set_both_wallpaper_button" }.run { click(getCentrePoint()) }
                        onTaskLog("Apply wallpaper to home and lock screen.")
                    }
                    AndroidDeviceModel.KX20 -> {
                        openInputAndEnterSystemSettings("wallpaper")
                        dumpUI().first { it.text == "Wallpaper" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.text == "Wallpapers" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.text == "My photos" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.contentDesc == "Show roots" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.text == "Downloads" }.run { click(getCentrePoint()) }
                        dumpUI().firstOrNull { it.text == "Portrait 800x1200.jpg" }.let {
                            if (it == null) {
                                throw TaskFailedToExecException(
                                    getId(),
                                    "please make sure the Portrait 800x1200.jpg has been placed under /sdcard/Download folder!"
                                )
                            }
                            click(it.getCentrePoint())
                            onTaskLog("Pick Portrait 800x1200.jpg as wallpaper.")
                        }
                        dumpUI().first { it.text == "Set wallpaper" }.run { click(getCentrePoint()) }
                        dumpUI().first { it.text == "Home screen and lock screen" }.run {
                            click(getCentrePoint())
                            onTaskLog("Apply wallpaper to home and lock screen.")
                        }
                    }
                }
            }
        }
    }
}