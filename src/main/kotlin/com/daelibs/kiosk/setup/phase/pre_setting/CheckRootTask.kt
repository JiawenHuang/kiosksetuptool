package com.daelibs.kiosk.setup.phase.pre_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService
import kotlinx.coroutines.delay
import java.io.File

class CheckRootTask(taskIndex: Int) : CmdTask(taskIndex, "Check kiosk root status") {
    private suspend fun isDeviceRooted(runCommandSync: suspend (String) -> String): Boolean {
        val su = "su"
        val locations = arrayOf(
            "/system/bin/", "/system/xbin/", "/sbin/", "/system/sd/xbin/",
            "/system/bin/failsafe/", "/data/local/xbin/", "/data/local/bin/", "/data/local/",
            "/system/sbin/", "/usr/bin/", "/vendor/bin/"
        )
        for (location in locations) {
            val isFileNotExist = runCommandSync(
                "shell ls -l '${location + su}'"
            ).contains("no such file", true)
            if (!isFileNotExist) {
                return true
            }
        }
        return false
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                runCommandSync(getId(), concatCmd("shell svc power stayon usb"))
                onTaskLog("Set screen stay on when usb plugged.")
                runCommandSync(
                    getId(),
                    concatCmd("shell content insert --uri content://settings/system --bind name:s:accelerometer_rotation --bind value:i:0")
                )
                runCommandSync(
                    getId(),
                    concatCmd("shell content insert --uri content://settings/system --bind name:s:user_rotation --bind value:i:1")
                )
                onTaskLog("Force screen orientation to landscape.")
                if (model == AndroidDeviceModel.P90) {
                    val isDeviceRooted = isDeviceRooted { cmd ->
                        runCommandSync(getId(), concatCmd(cmd))
                    }
                    if (!isDeviceRooted) {
                        throw TaskFailedToExecException(getId(), "device is not rooted yet!")
                    } else {
                        onTaskLog("Device has root permission.")
                    }
                    return
                }
                val hasRootCheckAppInstalled =
                    runCommandSync(getId(), concatCmd("shell pm path com.joeykrim.rootcheck")).isNotBlank()
                if (hasRootCheckAppInstalled) {
                    onTaskLog("RootChecker app already installed, skip to install.")
                    runCommandSync(getId(), concatCmd("shell am force-stop com.joeykrim.rootcheck"))
                } else {
                    val rootCheckerAppPath =
                        runCommandSync(getId(), concatCmd("shell ls /sdcard/Download/")).let { cmdRes ->
                            "/sdcard/Download/${
                                cmdRes.split("\n").map { it.trimEnd() }.first { it.contains("rootchecker", true) }
                            }"
                        }
                    val installResult = runCommandSync(
                        getId(),
                        concatCmd(
                            "shell pm install -g -r \"$rootCheckerAppPath\""
                        )
                    ).contains("Success")
                    onTaskLog("Install RootChecker app ${parseResult(installResult)}.")
                    if (!installResult) {
                        throw TaskFailedToExecException(getId(), "failed to install RootChecker app!")
                    }
                }
                runCommandSync(
                    getId(),
                    concatCmd("shell am start -n com.joeykrim.rootcheck/com.joeykrim.rootcheck.MainActivity")
                )
                delay(1000L)
                dumpUI().run {
                    if (firstOrNull { it.text.contains("Rate on google play", true) } != null) {
                        firstOrNull { it.text.contains("no", true) }?.run { click(getCentrePoint()) }
                    }
                }
                dumpUI().firstOrNull { it.text == "AGREE" }?.run { click(getCentrePoint()) }
                dumpUI().firstOrNull { it.text == "GET STARTED" }?.run {
                    click(getCentrePoint())
                }
                dumpUI().first { it.resId == "com.joeykrim.rootcheck:id/txtVerifyRoot" }.run {
                    click(getCentrePoint())
                }
                val hasRootAccess = dumpUI().firstOrNull { it.text.contains("Congratulations", true) } != null
                onTaskLog("Check root access ${parseResult(hasRootAccess)}.")
                if (!hasRootAccess) {
                    throw TaskFailedToExecException(getId(), "device is not rooted yet!")
                } else {
                    runCommandSync(getId(), concatCmd("shell service call window 1 i32 4939"))
                }
            }
        }
    }
}