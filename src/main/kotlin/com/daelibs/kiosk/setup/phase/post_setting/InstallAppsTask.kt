package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class InstallAppsTask(taskIndex: Int) : CmdTask(taskIndex, "Install apps") {
    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                runCommandSync(getId(), concatCmd("shell ls /sdcard/Download/")).let { cmdRes ->
                    cmdRes.split("\n").map { it.trimEnd() }
                }.filter { !it.contains("rootchecker", true) && it.endsWith(".apk", true) }.forEach { apkName ->
                    val apkFilePath = "/sdcard/Download/$apkName"
                    when(model){
                        AndroidDeviceModel.P90 -> {
                            runCommandSync(getId(), concatCmd("shell cp \"$apkFilePath\" /data/local/tmp"))
                            onTaskLog("Copy $apkFilePath to /data/local/tmp.")
                            runCommandSync(getId(), concatCmd("shell pm install \"/data/local/tmp/${apkName}\"")).let {
                                val installSuccess = it.contains("Success")
                                onTaskLog("Install /sdcard/Download/${apkName} ${parseResult(installSuccess)}.")
                                if (!installSuccess) {
                                    throw TaskFailedToExecException(getId(), "install failed: $it")
                                }
                            }
                        }
                        AndroidDeviceModel.KX20 -> {
                            runCommandSync(getId(), concatCmd("shell pm install -g -r \"/sdcard/Download/${apkName}\"")).let {
                                val installSuccess = it.contains("Success")
                                onTaskLog("Install /sdcard/Download/${apkName} ${parseResult(installSuccess)}.")
                                if (!installSuccess) {
                                    throw TaskFailedToExecException(getId(), "install failed: $it")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}