package com.daelibs.kiosk.setup.phase.pre_setting

import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.phase.SetupPhase
import kotlin.reflect.KClass

class PreSettingPhase : SetupPhase("Pre Settings") {
    override val allTaskClasses: List<KClass<out CmdTask>>
        get() = listOf(
            FilesTransferTask::class,
            CheckRootTask::class,
            CheckInternetTask::class
        )
}