package com.daelibs.kiosk.setup.phase

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.service.AdbCmdService

abstract class CmdTask(private val taskIndex: Int, private val taskDescription: String) {
    var completed = false
    protected fun parseResult(result: Boolean) = if (result) "success" else "failed"

    suspend fun execute(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit,
        onTaskStart: (Int, String) -> Unit,
        onTaskDone: (Int) -> Unit
    ) {
        if (completed) {
            return
        }
        onTaskStart(taskIndex, taskDescription)
        val taskResult = runCatching {
            performTask(config, cmdService, deviceState, onTaskLog)
        }
        completed = true
        if (taskResult.isSuccess) {
            onTaskDone(taskIndex)
        } else {
            throw taskResult.exceptionOrNull()!!
        }
    }

    abstract suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    )
}