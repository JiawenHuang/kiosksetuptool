package com.daelibs.kiosk.setup.phase.app_setting

import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.phase.SetupPhase
import kotlin.reflect.KClass

class AppSettingPhase : SetupPhase("App Settings") {

    override val allTaskClasses: List<KClass<out CmdTask>>
        get() = listOf(AdjustMessagingAppTask::class)

}