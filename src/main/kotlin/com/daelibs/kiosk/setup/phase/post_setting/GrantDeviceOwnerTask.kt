package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class GrantDeviceOwnerTask(taskIndex: Int) : CmdTask(taskIndex, "Grant DeviceOwner permission") {
    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                val hasDaelockInstalled =
                    runCommandSync(getId(), concatCmd("shell pm path com.daelibs.daelock")).isNotBlank()
                if (!hasDaelockInstalled) {
                    throw TaskFailedToExecException(
                        getId(),
                        "please make sure Daelock has been installed on the device!"
                    )
                }
                runCommandSync(
                    getId(),
                    concatCmd("shell dpm set-device-owner com.daelibs.daelock/.framework.receiver.DeviceOwnerPermissionReceiver")
                )
                onTaskLog("Grant device owner permission to Daelock.")
            }
        }
    }
}