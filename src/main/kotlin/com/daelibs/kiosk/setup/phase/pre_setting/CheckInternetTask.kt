package com.daelibs.kiosk.setup.phase.pre_setting

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class CheckInternetTask(taskIndex: Int) : CmdTask(taskIndex, "Check internet connection") {
    companion object {
        val PATTERN_CHECK_INTERNET = "unknown host www.google.com".toRegex()
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.runCommandSync(deviceState.getId(), deviceState.concatCmd("shell ping -c 4 www.google.com")).let {
            val isDisconnected = PATTERN_CHECK_INTERNET.containsMatchIn(it)
            onTaskLog("Kiosk is ${if (isDisconnected) "disconnected" else "connected"} to internet.")
            if (isDisconnected) {
                throw TaskFailedToExecException(deviceState.getId(), "kiosk not connected to internet.")
            }
        }
    }
}