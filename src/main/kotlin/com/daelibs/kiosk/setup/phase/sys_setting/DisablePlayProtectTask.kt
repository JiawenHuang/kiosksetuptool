package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

@Deprecated("Replaced by adb command")
class DisablePlayProtectTask(taskIndex: Int) : CmdTask(taskIndex, "Disable Play Protect") {
    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                openSystemSettings()
                click(1890, 103)
                input("security")
                navBack()
                dumpUI().first { it.text.contains("Security & Location", true) }.run { click(getCentrePoint()) }
                navDown()
                navClick()
                //there are possibly 2 scenarios
                val playProtectScreen = dumpUI()
                val settingNode = playProtectScreen.firstOrNull {
                    it.contentDesc == "Settings"
                }
                if (settingNode == null) {
                    //scenario 1, directly disable it
                    playProtectScreen.first { it.resId == "com.google.android.gms:id/toggle" }.let {
                        if (it.isChecked()) {
                            click(it.getCentrePoint())
                            click(dumpUI().first { it.text == "OK" }.getCentrePoint())
                        }
                    }
                } else {
                    //scenario 2, open settings screen
                    click(settingNode.getCentrePoint())
                    dumpUI().first { it.resId == "android:id/switch_widget" }.let {
                        if (it.isChecked()) {
                            click(it.getCentrePoint())
                            click(dumpUI().first { it.text == "Turn off" }.getCentrePoint())
                        }
                    }
                }
            }
        }
    }
}