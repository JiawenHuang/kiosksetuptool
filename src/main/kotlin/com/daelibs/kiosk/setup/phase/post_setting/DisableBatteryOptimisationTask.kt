package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class DisableBatteryOptimisationTask(taskIndex: Int) :
    CmdTask(taskIndex, "Disable battery optimisation") {
    companion object {
        private fun getBatteryOptiPacakges(model: AndroidDeviceModel): Map<String, String> {
            val pkgs = mutableMapOf(
                "Daelock" to "com.daelibs.daelock",
                "ServiceKiosk" to "daelibs.com.servicekiosk",
                "TeamViewer" to "com.teamviewer.host.market",
                "Bluetooth" to "com.android.bluetooth",
            )
            if (model == AndroidDeviceModel.KX20) {
                pkgs["BT Tool"] = "com.mediatek.bluetooth.dtt"
            }
            return pkgs.toMap()
        }
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                getBatteryOptiPacakges(model).forEach { (name, pkg) ->
                    val cmdRes = runCommandSync(
                        getId(),
                        concatCmd("shell dumpsys deviceidle whitelist +$pkg")
                    )
                    onTaskLog("Disable battery optimisation for $name.")
                    if (!cmdRes.contains("Added", true)) {
                        throw TaskFailedToExecException(
                            getId(),
                            "failed to disable battery optimisation for $name, reason: $cmdRes"
                        )
                    }
                }
            }
        }
    }
}