package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class MiscConfigTask(taskIndex: Int) : CmdTask(taskIndex, "Config misc") {
    companion object {
        private fun getScreenBrightness(model: AndroidDeviceModel) =
            when(model){
                AndroidDeviceModel.P90 -> 230
                AndroidDeviceModel.KX20 -> 90
            }

        private fun getConfigCmds(model: AndroidDeviceModel) = mapOf(
            "Disable Data Saver mode" to "cmd netpolicy set restrict-background false",
            "Disable intelligent power saving mode" to "settings put system background_power_saving_enable 0",
            "Set screen off timeout to never" to "settings put system screen_off_timeout 0",
            "Set screen brightness to 90%" to "settings put system screen_brightness ${getScreenBrightness(model)}",
            "Disable screen saver" to "settings put secure screensaver_enabled 0",
            "Allow to install non-market apps" to "settings put secure install_non_market_apps 1",
            "Set voice recognition server to GoogleRecognitionService" to "settings put secure voice_recognition_service com.google.android.googlequicksearchbox/com.google.android.voicesearch.serviceapi.GoogleRecognitionService",
            "Enable bluetooth" to "settings put global bluetooth_on 1",
            "Disable wifi" to "svc wifi disable",
            "Enable mobile data" to "settings put global mobile_data 1",
            "Disable Google Play Protect" to "settings put global package_verifier_user_consent -1"
        )
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                getConfigCmds(model).forEach { (desc, cmd) ->
                    runCommandSync(getId(), concatCmd("shell $cmd"))
                    onTaskLog(desc)
                }
                runCommandSync(getId(), concatCmd("shell setprop persist.sys.timezone \"${config.timezone}\"")).also {
                    onTaskLog("Set timezone to ${config.timezone}.")
                }
            }
        }
    }
}