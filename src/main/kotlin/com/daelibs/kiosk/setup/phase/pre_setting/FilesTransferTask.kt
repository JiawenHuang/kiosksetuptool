package com.daelibs.kiosk.setup.phase.pre_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService
import java.io.File

class FilesTransferTask(taskIndex: Int) : CmdTask(taskIndex, "Transfer all the files under files folder") {
    companion object {
        private val PATTERN_FILE_TRANSFER = "pushed".toRegex()
    }

    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                File(Config.filesFolderPath).listFiles()!!.forEach {
                    val isFileNotExist = runCommandSync(
                        getId(),
                        concatCmd("shell ls -l '/sdcard/Download/${it.name}'")
                    ).contains("no such file", true)
                    if (!isFileNotExist) {
                        onTaskLog("File ${it.absolutePath} already transferred to /sdcard/download/, skipped.")
                    } else {
                        runCommandSync(
                            getId(),
                            "adb#-s#${deviceState.serial}#-t#${deviceState.port}#push#${it.absolutePath}#/sdcard/download/",
                            "#"
                        ).let { res ->
                            val isTransferred = PATTERN_FILE_TRANSFER.containsMatchIn(res)
                            if (!isTransferred) {
                                throw TaskFailedToExecException(getId(), "transfer file: ${it.name} to kiosk failed!")
                            } else {
                                if (it.name.endsWith(".jpg")) {
                                    runCommandSync(
                                        getId(),
                                        concatCmd("shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d 'file:///sdcard/Download/Portrait 800x1200.jpg'")
                                    )
                                }
                                onTaskLog("Transferred file from ${it.absolutePath} to directory /sdcard/download/.")
                            }
                        }
                    }
                }
            }
        }
    }
}