package com.daelibs.kiosk.setup.phase

import com.daelibs.kiosk.setup.util.FileLogger
import java.io.File

class GetKioskDeviceInitLogTask {
    fun execute(kioskId: String) = File(".").absolutePath.let { workingDir ->
        buildString {
            append(FileLogger.logTimestampFormat.format(System.currentTimeMillis()))
            append("Kiosk($kioskId) ready, current working directory: ${workingDir.trimEnd()}")
            appendLine()
        }
    }
}