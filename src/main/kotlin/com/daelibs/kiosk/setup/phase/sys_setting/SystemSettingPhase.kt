package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.phase.SetupPhase
import kotlin.reflect.KClass

class SystemSettingPhase : SetupPhase("System Settings") {

    override val allTaskClasses: List<KClass<out CmdTask>>
        get() = listOf(
            CheckDoNotDisturbTask::class,
            CheckPriorityOnlyAllowsTask::class,
            SetWallpaperTask::class,
            AdjustLanguageTask::class,
            AdjustVoiceInputTask::class,
            AdjustScheduledPowerOnOffTimeTask::class
        )
}
