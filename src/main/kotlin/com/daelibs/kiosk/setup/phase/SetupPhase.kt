package com.daelibs.kiosk.setup.phase

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.service.AdbCmdService
import kotlin.reflect.KClass
import kotlin.reflect.full.primaryConstructor

abstract class SetupPhase(val phaseName: String) {
    private var allCmdTasks: List<CmdTask> = emptyList()

    abstract val allTaskClasses: List<KClass<out CmdTask>>
    val taskCount: Int
        get() = allTaskClasses.size

    fun getCompletedTasks() = allCmdTasks.count { it.completed }

    suspend fun setup(
        config: Config,
        adbCmdService: AdbCmdService,
        kioskState: AndroidDeviceUiState,
        maxTaskIndex: Int,
        onTaskLog: (String) -> Unit,
        onTaskStart: (Int, String) -> Unit,
        onTaskDone: (Int) -> Unit
    ) {
        if (getCompletedTasks() == taskCount) {
            return
        }
        if (allCmdTasks.isEmpty()) {
            allCmdTasks =
                allTaskClasses.mapIndexed { index, kClass -> kClass.primaryConstructor!!.call(maxTaskIndex + index + 1) }
        }
        allCmdTasks.filterNot { it.completed }.forEach {
            it.execute(config, adbCmdService, kioskState, onTaskLog, onTaskStart, onTaskDone)
        }
    }
}