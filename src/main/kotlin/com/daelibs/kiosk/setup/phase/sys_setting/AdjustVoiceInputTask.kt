package com.daelibs.kiosk.setup.phase.sys_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class AdjustVoiceInputTask(taskIndex: Int) : CmdTask(taskIndex, "Adjust VoiceInput") {
    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                when (model) {
                    AndroidDeviceModel.P90 -> {
                        openInputAndEnterSystemSettings("Language")
                        dumpUI().run {
                            first { it.resId == "com.android.settings:id/settings_button" }.run {
                                click(getCentrePoint())
                                navDown()
                                navClick()
                                while (true) {
                                    val enAuNode = dumpUI().firstOrNull { it.text == "English (Australia)" }
                                    if (enAuNode != null) {
                                        click(enAuNode.getCentrePoint())
                                        dumpUI().firstOrNull { it.resId == "com.google.android.tts:id/voice_entry_download_button" }.let {
                                            if (it == null) {
                                                onTaskLog("English (Australia) TTS voice data already exists.")
                                            } else {
                                                onTaskLog("Download English (Australia) TTS voice data now...")
                                                click(it.getCentrePoint())
                                            }
                                        }
                                        navBack()
                                        break
                                    }
                                    swipe(800, 1000, 800, 480)
                                }
                                repeat(2) {
                                    navBack()
                                }
                            }
                            first{it.text == "Language"}.run {
                                click(getCentrePoint())
                                while (true) {
                                    val enAuNode = dumpUI().firstOrNull { it.text == "English (Australia)" }
                                    if (enAuNode != null) {
                                        click(enAuNode.getCentrePoint())
                                        onTaskLog("Set English (Australia) as default language.")
                                        break
                                    }
                                    swipe(800, 900, 800, 200)
                                }
                            }

                        }
                    }

                    AndroidDeviceModel.KX20 -> {
                        openInputAndEnterSystemSettings("language")
                        repeat(2) {
                            navDown()
                            navClick()
                        }
                        val langVoiceUI = dumpUI()
                        // Set en/AU primary
                        val isOldVersion = langVoiceUI.firstOrNull { it.text == "Primary language" } != null
                        onTaskLog("Check Google Voice settings if old version: $isOldVersion.")
                        if (isOldVersion) {
                            val isPrimarySet =
                                langVoiceUI.firstOrNull { it.text == "Primary language: English (Australia)" } != null
                            onTaskLog("Check English (Australia) if primary: $isPrimarySet.")
                            if (!isPrimarySet) {
                                langVoiceUI.first { it.text == "Languages" }.run { click(getCentrePoint()) }
                                swipe(1018, 778, 1018, 590)
                                val langPickUI = dumpUI()
                                val enAuItemRow = langPickUI.first { it.text == "English (Australia)" }
                                var hasPerformCheck = false
                                onTaskLog("Check English (Australia) if checked: ${enAuItemRow.isChecked()}.")
                                if (!enAuItemRow.isChecked()) {
                                    hasPerformCheck = true
                                    click(enAuItemRow.getCentrePoint())
                                }
                                langPickUI.first { it.text == "OK" }.run { click(getCentrePoint()) }
                                if (hasPerformCheck) {
                                    dumpUI().firstOrNull { it.text == "English (Australia)" }
                                        ?.run { click(getCentrePoint()) }
                                    onTaskLog("Set English (Australia) as primary after checked on langPick UI.")
                                } else {
                                    langVoiceUI.first { it.text == "Primary language" }.run { click(getCentrePoint()) }
                                    dumpUI().first { it.text == "English (Australia)" }.run { click(getCentrePoint()) }
                                    onTaskLog("Set English (Australia) as primary after checked on separate Primary Language UI.")
                                }
                            } else {

                            }
                        } else {
                            val isDefaultSet =
                                langVoiceUI.firstOrNull() { it.text == "Default language: English (Australia)" } != null
                            onTaskLog("Check English (Australia) if default primary: $isDefaultSet.")
                            if (!isDefaultSet) {
                                langVoiceUI.first { it.text == "Languages" }.run { click(getCentrePoint()) }
                                var langPickScreen = dumpUI()
                                while (true) {
                                    langPickScreen.filter { it.resId == "android:id/text1" }.forEach {
                                        if (it.text == "English (Australia)") {
                                            if (!it.isChecked()) {
                                                click(it.getCentrePoint())
                                                onTaskLog("Set English (Australia) as primary.")
                                            }
                                        } else {
                                            if (it.isChecked()) {
                                                click(it.getCentrePoint())
                                                onTaskLog("Deselect ${it.text}.")
                                            }
                                        }
                                    }
                                    swipe(800, 1000, 800, 480)
                                    val updatedLangPickScreen = dumpUI()
                                    if (updatedLangPickScreen == langPickScreen) {
                                        break
                                    }
                                    langPickScreen = updatedLangPickScreen
                                }
                                langPickScreen.first { it.text == "SAVE" }.run {
                                    click(getCentrePoint())
                                    onTaskLog("Save changes.")
                                }
                            }
                            // Check and download offline pack
                            langVoiceUI.first { it.text == "Offline speech recognition" }
                                .run { click(getCentrePoint()) }
                            val downloadLangUI = dumpUI()
                            val isEnAuDownloaded =
                                downloadLangUI.firstOrNull { it.text == "English (Australia)" } != null
                            onTaskLog("Check English (Australia) offline recognition pack if downloaded: $isEnAuDownloaded.")
                            if (!isEnAuDownloaded) {
                                downloadLangUI.first { it.text == "All" }.run { click(getCentrePoint()) }
                                dumpUI().first { it.text == "English (Australia)" }.run { click(getCentrePoint()) }
                                dumpUI().firstOrNull { it.text == "DOWNLOAD" }.let {
                                    if (it != null) {
                                        click(it.getCentrePoint())
                                        onTaskLog("Start downloading English (Australia) offline recognition pack now.")
                                    } else {
                                        val isDownloadingNow =
                                            dumpUI().firstOrNull { it.text == "STOP DOWNLOAD" } != null
                                        onTaskLog("Already started downloading English (Australia) offline recognition pack.")
                                        navBack()
                                    }
                                }
                            } else {

                            }
                        }
                    }
                }
            }
        }
    }
}