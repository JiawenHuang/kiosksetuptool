package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class AllowAppsToInstallUpdatesTask(taskIndex: Int) : CmdTask(taskIndex, "Allow self updates for apps") {
    companion object {
        private val PKGS = mapOf(
            "Daelock" to "com.daelibs.daelock",
            "ServiceKiosk" to "daelibs.com.servicekiosk",
            "TeamViewer" to "com.teamviewer.host.market"
        )
    }

    override suspend fun performTask(
        config: Config,
        cmdService: AdbCmdService,
        deviceState: AndroidDeviceUiState,
        onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                PKGS.forEach { (name, pkg) ->
                    runCommandSync(
                        getId(), concatCmd("shell appops set $pkg REQUEST_INSTALL_PACKAGES allow")
                    )
                    onTaskLog("Allow $name to install updates.")
                }
            }
        }
    }
}