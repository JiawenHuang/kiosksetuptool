package com.daelibs.kiosk.setup.phase.app_setting

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class AdjustMessagingAppTask(taskIndex: Int) : CmdTask(taskIndex, "Adjust Messaging app") {
    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                when (model) {
                    AndroidDeviceModel.P90 -> {
                        runCommandSync(
                            getId(),
                            concatCmd("shell am start -S -W -n com.google.android.apps.messaging/.ui.appsettings.ApplicationSettingsActivity")
                        )
                        dumpUI().run {
                            first { it.resId == "com.google.android.apps.messaging:id/switchWidget" }.run {
                                if (isChecked()) {
                                    click(
                                        getCentrePoint()
                                    )
                                    onTaskLog("Disable hear outgoing message sounds.")
                                }
                            }
                            first { it.text == "Notifications" }.run {
                                click(getCentrePoint())
                                dumpUI().first { it.resId == "com.android.settings:id/switch_widget" }
                                    .run {
                                        if (isChecked()) {
                                            click(getCentrePoint())
                                            onTaskLog("Disable all messages notifications.")
                                        }
                                    }
                                navBack()
                            }
                        }
                    }

                    AndroidDeviceModel.KX20 -> {
                        runCommandSync(getId(), concatCmd("shell am force-stop com.android.mms"))
                        runCommandSync(
                            getId(),
                            concatCmd("shell am start -S -W -n com.android.mms/com.mediatek.setting.SettingListActivity")
                        )
                        val messagingAppSettingUI = dumpUI()
                        messagingAppSettingUI.first { it.text == "Notifications" }.run { click(getCentrePoint()) }
                        dumpUI().filter { it.resId == "android:id/checkbox" }.reversed()
                            .forEachIndexed { index, uiNode ->
                                if (uiNode.isChecked()) {
                                    click(uiNode.getCentrePoint())
                                    onTaskLog("Switch ${if (index == 0) "Popup notification" else if (index == 1) "Vibrate" else "Message notifications"} to off")
                                }
                            }
                        navBack()
                        messagingAppSettingUI.first { it.text == "General" }.run { click(getCentrePoint()) }
                        val generalSettingsUI = dumpUI()
                        generalSettingsUI.first { it.resId == "android:id/checkbox" }.run {
                            if (!isChecked()) {
                                click(getCentrePoint())
                                onTaskLog("Switch delete old messages to true")
                            }
                        }
                        generalSettingsUI.first { it.text == "Text-message limit" }.run { click(getCentrePoint()) }
                        input("10")
                        dumpUI().first { it.text == "Set" }.run { click(getCentrePoint()) }
                        onTaskLog("Set text message limit to 10")
                        generalSettingsUI.first { it.text == "Multimedia message limit" }
                            .run { click(getCentrePoint()) }
                        input("10")
                        dumpUI().first { it.text == "Set" }.run { click(getCentrePoint()) }
                        onTaskLog("Set multimedia message limit to 10")
                        generalSettingsUI.last { it.resId == "android:id/checkbox" }.run {
                            if (isChecked()) {
                                click(getCentrePoint())
                                onTaskLog("Switch enable WAP PUSH to false")
                            }
                        }
                        navBack()
                        messagingAppSettingUI.first { it.text.contains("MMS") }.run { click(getCentrePoint()) }
                        //second from last
                        dumpUI().filter { it.resId == "android:id/checkbox" }[4].run {
                            if (isChecked()) {
                                click(getCentrePoint())
                                onTaskLog("Switch auto-retrieve to false")
                            }
                        }
                    }
                }
            }
        }

    }
}