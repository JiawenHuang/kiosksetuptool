package com.daelibs.kiosk.setup.phase.post_setting

import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.phase.CmdTask
import com.daelibs.kiosk.setup.service.AdbCmdService

class RebootTask(taskIndex: Int) : CmdTask(taskIndex, "Reboot the device and ready to start TeamViewer setup") {
    override suspend fun performTask(
        config: Config, cmdService: AdbCmdService, deviceState: AndroidDeviceUiState, onTaskLog: (String) -> Unit
    ) {
        cmdService.run {
            deviceState.run {
                runCommandSync(getId(), concatCmd("reboot"))
            }
        }
    }
}