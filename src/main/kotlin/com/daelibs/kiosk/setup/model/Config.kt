package com.daelibs.kiosk.setup.model

import kotlinx.serialization.Serializable


@Serializable
data class Config(val timezone: String, val onTime: String, val offTime: String) {
    companion object {
        const val filesFolderPath = "config/files"
        const val logFolderPath = "config/logs/"
        const val configFilePath = "config/config.json"

        val DEFAULT = Config(timezone = "Australia/Brisbane", onTime = "05:30", "19:00")
    }
}
