package com.daelibs.kiosk.setup.model.exception

data class AdbCmdFailedToExecException(val kioskId: String, override val cause: Throwable?) : Throwable()