package com.daelibs.kiosk.setup.model

import com.daelibs.kiosk.setup.phase.SetupPhase

/**
 * READY -> "START"
 * RUNNING -> "STOP"
 * ERROR -> "SKIP"
 * FINISHED -> "RESTART"
 */
enum class KioskSetupState {
    READY, RUNNING, ERROR, FINISHED
}

data class AndroidDeviceUiState(
    val serial: String,
    val model: AndroidDeviceModel,
    val port: String,
    val buildPhases: List<SetupPhase>,
    val completedTasks: Int,
    val logs: List<String>,
    val setupState: KioskSetupState,
    val checked: Boolean,
    var jobStartTime: Long,
) {
    val totalTasksCount = buildPhases.sumOf { it.taskCount }
    fun getSetupStateText() = when (setupState) {
        KioskSetupState.READY -> "START"
        KioskSetupState.RUNNING -> "STOP"
        KioskSetupState.ERROR -> "SKIP"
        KioskSetupState.FINISHED -> "RESETUP"
    }

    fun getLatestMaxTaskIndex(phaseIndex: Int): Int {
        var index = 0
        var phaseTmpIndex = 0
        while (phaseTmpIndex < phaseIndex) {
            index += buildPhases[phaseTmpIndex].taskCount
            phaseTmpIndex++
        }
        return index
    }

    fun getProgress() = completedTasks.toFloat() / totalTasksCount
    fun getTitle() = "Serial: $serial Model: $model TransportId: $port"
    fun getRowTitle() = if (serial.contains("0123456789ABCDEF", true)) "$serial/$port" else serial
    fun getId() = getRowTitle()
    fun getProgressPercentage() = "${(getProgress() * 100).toInt()}%"
    fun concatCmd(cmd: String) = "adb -s $serial -t $port $cmd"
}