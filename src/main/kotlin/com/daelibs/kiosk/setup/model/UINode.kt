package com.daelibs.kiosk.setup.model

import java.awt.Point

data class UINode(
    val index: String,
    val text: String,
    val resId: String,
    val contentDesc: String,
    val checked: String,
    val selected: String,
    val bounds: String
) {
    companion object {
        val boundsRegex = "\\d{1,4}\\,\\d{1,4}".toRegex()
    }

    fun isChecked() = "true" == checked
    fun isSelected() = "true" == selected

    fun getCentrePoint(): Point {
        val boundsPoints = parseBounds()
        val pointLeftTop = boundsPoints[0]
        val pointRightBot = boundsPoints[1]
        return Point(
            pointLeftTop.x + (pointRightBot.x - pointLeftTop.x) / 2,
            pointLeftTop.y + (pointRightBot.y - pointLeftTop.y) / 2
        )
    }

    fun parseBounds() = boundsRegex.findAll(bounds).map {
        val pointVal = it.groupValues[0].split(",")
        Point(pointVal[0].toInt(), pointVal[1].toInt())
    }.toList()
}
