package com.daelibs.kiosk.setup.model

import androidx.compose.material.SnackbarHostState

data class SetupUiState(
    val kioskDevices: List<AndroidDeviceUiState>,
    val selectedKioskId: String? = null,
    val isBatchMode: Boolean = false,
    val showDialog: Boolean = false,
    val menuState: SetupUiMenuState = SetupUiMenuState.DEFAULT,
    val snackBarState: SnackbarHostState = SnackbarHostState()
) {
    companion object {
        val DEFAULT = SetupUiState(kioskDevices = emptyList())
    }
}