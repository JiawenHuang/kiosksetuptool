package com.daelibs.kiosk.setup.model

enum class AndroidDeviceModel(model: String){
    P90("P90"), KX20("KX20")
}

data class AndroidDevice(
    val serial: String,
    val product: String,
    val model: AndroidDeviceModel,
    val device: String,
    val transportId: String
) {
    fun getId() = "$serial/$transportId"
}