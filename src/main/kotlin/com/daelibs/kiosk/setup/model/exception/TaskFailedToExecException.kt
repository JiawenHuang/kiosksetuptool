package com.daelibs.kiosk.setup.model.exception

data class TaskFailedToExecException(val kioskId: String, override val message: String?) : Throwable()