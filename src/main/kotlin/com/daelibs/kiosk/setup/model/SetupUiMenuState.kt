package com.daelibs.kiosk.setup.model

data class SetupUiMenuState(
    val selectedSetupMode: Int = 0,
    val showAboutDialog: Boolean = false,
) {
    companion object {
        val DEFAULT = SetupUiMenuState()
    }
}
