package com.daelibs.kiosk.setup.service

import com.daelibs.kiosk.setup.model.AndroidDeviceModel
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.UINode
import com.daelibs.kiosk.setup.model.exception.AdbCmdFailedToExecException
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.util.FileLogger
import kotlinx.coroutines.delay
import kotlinx.coroutines.yield
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.awt.Point
import java.io.File
import java.util.concurrent.TimeUnit


class AdbCmdService {
    companion object {
        private val PATTERN_UI_DUMP_SUC = "UI hierchary dumped to: /data/local/tmp/ui_dump.xml".toRegex()
        private val PATTERN_UI_DUMP_PULL = "1 file pulled".toRegex()
    }

    suspend fun AndroidDeviceUiState.dumpUI(): List<UINode> {
        return runCommandDelay(
            getId(), concatCmd("shell uiautomator dump --compressed /data/local/tmp/ui_dump.xml")
        ).let { dumpRes ->
            if (!PATTERN_UI_DUMP_SUC.containsMatchIn(dumpRes)) {
                throw TaskFailedToExecException(getId(), "dump UI file to /data/local/tmp/ui_dump.xml failed!")
            } else {
                val uiDumpFilePath =
                    Config.logFolderPath + "ui_dump_${getId().split("/").joinToString(separator = "_")}.xml"
                runCommandDelay(
                    getId(), concatCmd("pull /data/local/tmp/ui_dump.xml $uiDumpFilePath")
                ).let { pullRes ->
                    if (!PATTERN_UI_DUMP_PULL.containsMatchIn(pullRes)) {
                        throw TaskFailedToExecException(getId(), "pull UI dump file failed!")
                    } else {
                        val xmlPullParser = XmlPullParserFactory.newInstance().newPullParser().apply {
                            setInput(File(uiDumpFilePath).reader())
                        }
                        val uiNodes = mutableListOf<UINode>()
                        var eventType = xmlPullParser.eventType
                        while (eventType != XmlPullParser.END_DOCUMENT) {
                            when (eventType) {
                                XmlPullParser.START_TAG -> {
                                    if (xmlPullParser.name == "node") {
                                        val attrIndex = xmlPullParser.getAttributeValue(0)
                                        val attrText = xmlPullParser.getAttributeValue(1)
                                        val attrResId = xmlPullParser.getAttributeValue(2)
                                        val attrContentDesc = xmlPullParser.getAttributeValue(5)
                                        val attrChecked = xmlPullParser.getAttributeValue(7)
                                        val attrSelected = xmlPullParser.getAttributeValue(15)
                                        val attrBounds = xmlPullParser.getAttributeValue(16)
                                        uiNodes.add(
                                            UINode(
                                                attrIndex,
                                                attrText,
                                                attrResId,
                                                attrContentDesc,
                                                attrChecked,
                                                attrSelected,
                                                attrBounds
                                            )
                                        )
                                    }
                                }
                            }
                            eventType = xmlPullParser.next()
                        }
                        uiNodes
                    }
                }
            }
        }
    }

    suspend fun AndroidDeviceUiState.openSystemSettings() =
        runCommandDelay(
            getId(),
            concatCmd("shell am start -W -S -n com.android.settings/com.android.settings.Settings")
        )

    suspend fun AndroidDeviceUiState.openInputAndEnterSystemSettings(keyword: String) {
        openSystemSettings()
        when (model) {
            AndroidDeviceModel.P90 -> {
                click(1000, 144)
                input(keyword)
                navBack()
                val filterKeyword = keyword.replace("%s", " ")
                dumpUI().first { it.text == filterKeyword && it.resId == "android:id/title" }.run { click(getCentrePoint()) }
            }

            AndroidDeviceModel.KX20 -> {
                click(1890, 103)
                input(keyword)
                navBack()
                navDown()
                navClick()
            }
        }
    }

    suspend fun AndroidDeviceUiState.click(x: Int, y: Int) =
        runCommandDelay(getId(), concatCmd("shell input tap $x $y"))

    suspend fun AndroidDeviceUiState.click(point: Point) = click(point.x, point.y)

    private suspend fun AndroidDeviceUiState.longClick(x: Int, y: Int) =
        runCommandDelay(getId(), concatCmd("shell input swipe $x $y $x $y 800"))

    suspend fun AndroidDeviceUiState.longClick(point: Point) = longClick(point.x, point.y)

    suspend fun AndroidDeviceUiState.dragAndDrop(x1: Int, y1: Int, x2: Int, y2: Int) =
        runCommandDelay(getId(), concatCmd("shell input draganddrop $x1 $y1 $x2 $y2"))

    suspend fun AndroidDeviceUiState.swipe(x1: Int, y1: Int, x2: Int, y2: Int) =
        runCommandDelay(getId(), concatCmd("shell input swipe $x1 $y1 $x2 $y2"))

    suspend fun AndroidDeviceUiState.input(text: String) =
        runCommandDelay(getId(), concatCmd("shell input text $text"))

    suspend fun AndroidDeviceUiState.navBack() = runCommandDelay(getId(), concatCmd("shell input keyevent 4"))

    suspend fun AndroidDeviceUiState.navHome() = runCommandDelay(getId(), concatCmd("shell input keyevent 3"))

    suspend fun AndroidDeviceUiState.navUp() = runCommandDelay(getId(), concatCmd("shell input keyevent 19"))

    suspend fun AndroidDeviceUiState.navDown() = runCommandDelay(getId(), concatCmd("shell input keyevent 20"))

    suspend fun AndroidDeviceUiState.navLeft() = runCommandDelay(getId(), concatCmd("shell input keyevent 21"))

    suspend fun AndroidDeviceUiState.navRight() =
        runCommandDelay(getId(), concatCmd("shell input keyevent 22"))

    suspend fun AndroidDeviceUiState.navClick() =
        runCommandDelay(getId(), concatCmd("shell input keyevent 23"))

    suspend fun AndroidDeviceUiState.del() =
        runCommandDelay(getId(), concatCmd("shell input keyevent 67"), delayMills = 0L)

    suspend fun AndroidDeviceUiState.forwardDel() =
        runCommandDelay(getId(), concatCmd("shell input keyevent 112"), delayMills = 0L)

    suspend fun AndroidDeviceUiState.tab() =
        runCommandDelay(getId(), concatCmd("shell input keyevent 61"), delayMills = 0L)

    suspend fun runCommandDelay(
        kioskId: String?,
        cmd: String,
        delimiter: String = " ",
        delayMills: Long = 400L
    ): String = realRunCommand(kioskId, cmd, delimiter, delayMills = delayMills)

    suspend fun runCommandSync(kioskId: String?, cmd: String, delimiter: String = " "): String =
        runCommandDelay(kioskId, cmd, delimiter, 0L)

    private suspend fun realRunCommand(
        kioskId: String?,
        cmd: String,
        delimiter: String = " ",
        workingDir: File = File("."),
        timeoutAmount: Long = 30L,
        timeUnit: TimeUnit = TimeUnit.SECONDS,
        delayMills: Long = 400L
    ): String {
        val runResult = runCatching {
            yield()
            val cmdResult = ProcessBuilder(cmd.split(delimiter)).directory(workingDir).redirectErrorStream(true).start()
                .also { it.waitFor(timeoutAmount, timeUnit) }.inputStream.bufferedReader().readText()
            delay(delayMills)
            cmdResult
        }
        if (runResult.isFailure) {
            val exception = runResult.exceptionOrNull()!!
            FileLogger.log(kioskId, "ERROR for executing: $cmd")
            throw AdbCmdFailedToExecException(kioskId ?: "N/A", exception)
        } else {
            val cmdRes = runResult.getOrThrow().trimEnd().also {
                FileLogger.log(kioskId, "Exec: $cmd ${if (it.isBlank()) "" else "Result: $it"}")
            }
            return cmdRes
        }
    }
}