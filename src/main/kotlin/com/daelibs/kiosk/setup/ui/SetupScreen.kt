package com.daelibs.kiosk.setup.ui

import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.daelibs.kiosk.setup.model.KioskSetupState
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState

@Composable
fun SetupScreen(
    modifier: Modifier = Modifier,
    isBatchMode: Boolean,
    selectedTitle: String,
    selectedDeviceLogs: List<String>,
    deviceList: List<AndroidDeviceUiState>,
    toggleBatchMode: () -> Unit,
    onRefreshList: () -> Unit,
    onDeviceChecked: (String, Boolean) -> Unit,
    onDeviceSelected: (String) -> Unit,
    onSetupStateChange: (String, KioskSetupState) -> Unit,
) {
    Column(modifier) {
        TopAppBar(title = { Text(text = selectedTitle) }, actions = {
            IconButton(onClick = toggleBatchMode) {
                Icon(
                    painter = painterResource("checklist.svg"),
                    contentDescription = "Toggle batch mode",
                    tint = Color.White
                )
            }
            IconButton(onClick = onRefreshList) {
                Icon(imageVector = Icons.Default.Refresh, contentDescription = "Refresh kiosks", tint = Color.White)
            }
        })
        DeviceList(
            isBatchMode,
            deviceList,
            onDeviceChecked,
            onDeviceSelected,
            onSetupStateChange,
            modifier = Modifier.weight(1f)
        )
        Divider()
        DeviceLogList(selectedDeviceLogs, modifier = Modifier.height(200.dp))
    }
}

@Composable
private fun DeviceLogList(logs: List<String>, modifier: Modifier = Modifier) {
    Box(modifier = modifier) {
        val state = rememberLazyListState()
        LazyColumn(state = state) {
            items(items = logs, key = { it }) { log ->
                Text(
                    text = log,
                    modifier = Modifier.padding(16.dp, 8.dp),
                    style = TextStyle.Default.copy(
                        fontWeight = if (log.contains(
                                "phase",
                                true
                            )
                        ) FontWeight.Bold else FontWeight.Normal
                    )
                )
                if (logs.size > 1) {
                    Divider()
                }
            }
        }
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            adapter = rememberScrollbarAdapter(scrollState = state)
        )
        LaunchedEffect(logs.size) {
            state.animateScrollToItem(logs.size - 1)
        }
    }
}

@Composable
private fun DeviceList(
    isBatchMode: Boolean,
    deviceList: List<AndroidDeviceUiState>,
    onDeviceChecked: (String, Boolean) -> Unit,
    onDeviceSelected: (String) -> Unit,
    onSetupStateChange: (String, KioskSetupState) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier = modifier) {
        val state = rememberLazyListState()
        LazyColumn(modifier = modifier, state = state) {
            items(deviceList, key = { device -> device.getId() }) { device ->
                DeviceItemRow(isBatchMode, device, onDeviceChecked, onDeviceSelected, onSetupStateChange)
                Divider()
            }
        }
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            adapter = rememberScrollbarAdapter(scrollState = state)
        )
    }
}

@Composable
private fun DeviceItemRow(
    isBatchMode: Boolean,
    deviceItem: AndroidDeviceUiState,
    onDeviceChecked: (String, Boolean) -> Unit,
    onDeviceSelected: (String) -> Unit,
    onSetupStateChange: (String, KioskSetupState) -> Unit,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.clickable(onClick = { onDeviceSelected(deviceItem.getId()) })
    ) {
        Spacer(modifier = Modifier.width(16.dp))
        if (isBatchMode) {
            Checkbox(checked = deviceItem.checked, onCheckedChange = { onDeviceChecked(deviceItem.getId(), it) })
            Spacer(modifier = Modifier.width(8.dp))
        }
        Text(
            text = AnnotatedString(deviceItem.getRowTitle()),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.width(180.dp)
        )
        Spacer(modifier = Modifier.width(8.dp))
        LinearProgressIndicator(
            color = if (deviceItem.getProgress() == 1f) Color(0x00C853) else MaterialTheme.colors.primary,
            progress = deviceItem.getProgress(),
            modifier = Modifier.weight(1F)
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = deviceItem.getProgressPercentage()
        )
        Spacer(modifier = Modifier.width(8.dp))
        Button(
            modifier = Modifier.width(110.dp),
            onClick = { onSetupStateChange(deviceItem.getId(), deviceItem.setupState) },
        ) {
            Text(text = deviceItem.getSetupStateText())
        }
        Spacer(modifier = Modifier.width(16.dp))
    }
}