package com.daelibs.kiosk.setup.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.daelibs.kiosk.setup.model.AndroidDeviceUiState
import com.daelibs.kiosk.setup.model.Config
import com.daelibs.kiosk.setup.model.KioskSetupState
import com.daelibs.kiosk.setup.model.SetupUiState
import com.daelibs.kiosk.setup.model.exception.AdbCmdFailedToExecException
import com.daelibs.kiosk.setup.model.exception.TaskFailedToExecException
import com.daelibs.kiosk.setup.phase.GetConnectedKiosks
import com.daelibs.kiosk.setup.phase.GetKioskDeviceInitLogTask
import com.daelibs.kiosk.setup.phase.SetupPhase
import com.daelibs.kiosk.setup.phase.app_setting.AppSettingPhase
import com.daelibs.kiosk.setup.phase.post_setting.PostSettingPhase
import com.daelibs.kiosk.setup.phase.pre_setting.PreSettingPhase
import com.daelibs.kiosk.setup.phase.sys_setting.SystemSettingPhase
import com.daelibs.kiosk.setup.service.AdbCmdService
import com.daelibs.kiosk.setup.util.FileLogger
import com.daelibs.kiosk.setup.util.Util
import kotlinx.coroutines.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.io.File

class MainViewModel {

    private val appScope: CoroutineScope by lazy { CoroutineScope(SupervisorJob() + Dispatchers.Default) }
    private val adbCmdService by lazy { AdbCmdService() }
    private val kioskSetupJobMap by lazy { mutableMapOf<String, Job>() }
    private val appScopeExceptionHandler = CoroutineExceptionHandler { _, exception ->
        val kioskId = when (exception) {
            is TaskFailedToExecException -> exception.kioskId
            is AdbCmdFailedToExecException -> exception.kioskId
            else -> "N/A"
        }
        val isActivelyStop = exception.cause is CancellationException
        val haltReason = if (isActivelyStop) "actively stop" else {
            exception.message ?: exception.cause?.message ?: "unknown"
        }
        FileLogger.log(kioskId, "Exception happened:\n $exception")
        updateItemLog(kioskId, "Setup halt due to $haltReason")
        setSetupState(kioskId, if (isActivelyStop) KioskSetupState.FINISHED else KioskSetupState.ERROR)
    }
    private val config by lazy {
        File(Config.configFilePath).let {
            if (!it.exists()) {
                FileLogger.log(text = "Config doesn't exist, will use default Config!")
                Config.DEFAULT
            } else {
                Json.decodeFromStream<Config>(File(Config.configFilePath).inputStream()).also { configJson ->
                    FileLogger.log(text = "Load config: $configJson")
                }
            }
        }
    }

    var uiState by mutableStateOf(SetupUiState.DEFAULT)
        private set

    private val defaultBuildTasks
        get() = listOf(
            PreSettingPhase(),
            SystemSettingPhase(),
            AppSettingPhase(),
            PostSettingPhase()
        )

    suspend fun refresh() {
        kioskSetupJobMap.filter { it.value.isActive }.map { it.value }.forEach {
            it.cancel()
        }
        setState {
            copy(
                showDialog = false,
                kioskDevices = GetConnectedKiosks().execute(adbCmdService).map {
                    AndroidDeviceUiState(
                        it.serial,
                        it.model,
                        it.transportId,
                        defaultBuildTasks,
                        0,
                        listOf(GetKioskDeviceInitLogTask().execute(it.getId())),
                        KioskSetupState.READY,
                        false,
                        0L
                    )
                }.sortedBy { it.getTitle() })
        }
    }

    private fun setSetupState(kioskId: String, targetState: KioskSetupState) {
        setState {
            updateItem(kioskId) {
                it.copy(setupState = targetState)
            }
        }
    }

    fun toggleSetupState(kioskId: String, currentState: KioskSetupState) {
        val nextSetupState: KioskSetupState
        var buildPhases: List<SetupPhase>? = null
        var logs: List<String>? = null
        var completedTasks: Int? = null
        when (currentState) {
            KioskSetupState.READY -> {
                nextSetupState = KioskSetupState.RUNNING
                logs = listOf(GetKioskDeviceInitLogTask().execute(kioskId))
                buildPhases = defaultBuildTasks
                completedTasks = 0
            }

            KioskSetupState.RUNNING -> {
                nextSetupState = KioskSetupState.FINISHED
            }

            KioskSetupState.ERROR -> {
                nextSetupState = KioskSetupState.RUNNING
            }

            KioskSetupState.FINISHED -> {
                nextSetupState = KioskSetupState.RUNNING
                logs = listOf(GetKioskDeviceInitLogTask().execute(kioskId))
                buildPhases = defaultBuildTasks
                completedTasks = 0
            }
        }
        setState {
            updateItem(kioskId) {
                it.copy(
                    setupState = nextSetupState,
                    buildPhases = buildPhases ?: it.buildPhases,
                    completedTasks = completedTasks ?: it.completedTasks,
                    logs = logs ?: it.logs,
                )
            }
        }
        when (currentState) {
            KioskSetupState.READY -> {
                startSetup(kioskId)
            }

            KioskSetupState.RUNNING -> {
                stopSetup(kioskId)
            }

            KioskSetupState.ERROR -> {
                startSetup(kioskId)
            }

            KioskSetupState.FINISHED -> {
                startSetup(kioskId)
            }
        }
    }

    private fun startSetup(kioskId: String) {
        kioskSetupJobMap[kioskId] = appScope.launch(appScopeExceptionHandler) {
            try {
                val kioskState = uiState.kioskDevices.getKioskStateById(kioskId)
                if (kioskState.jobStartTime <= 0L) {
                    kioskState.jobStartTime = System.currentTimeMillis()
                }
                if (kioskState.completedTasks == 0) {
                    updateItemLog(kioskId, "Setup started!")
                } else {
                    updateItemLog(kioskId, "Setup resumed!")

                }
                val onTaskStart: (Int, String) -> Unit = { taskIndex, taskDesc ->
                    updateItemLog(kioskId, "Step $taskIndex/${kioskState.totalTasksCount}: $taskDesc ...")
                }
                val onTaskDone: (Int) -> Unit = { taskIndex ->
                    updateItemProgress(kioskId, taskIndex)
                }
                val onTaskLog: (String) -> Unit = { log ->
                    updateItemLog(kioskId, log)
                }
                kioskState.buildPhases.forEachIndexed { index, phase ->
                    if (phase.getCompletedTasks() == 0) {
                        updateItemLog(
                            kioskId,
                            "Phase ${index + 1}/${kioskState.buildPhases.size}: ${phase.phaseName} started!"
                        )
                    }
                    phase.setup(
                        config,
                        adbCmdService,
                        kioskState,
                        kioskState.getLatestMaxTaskIndex(index),
                        onTaskLog,
                        onTaskStart,
                        onTaskDone
                    )
                }
                setSetupState(kioskId, KioskSetupState.FINISHED)
                updateItemLog(
                    kioskId,
                    "Setup completed, time consumed: ${
                        Util.millis2FitTimeSpan(System.currentTimeMillis() - kioskState.jobStartTime).also {
                            kioskState.jobStartTime = 0L
                        }
                    }"
                )
            } catch (e: Throwable) {
                if (e is TaskFailedToExecException || e is AdbCmdFailedToExecException) {
                    throw e
                } else {
                    throw TaskFailedToExecException(kioskId, e.message)
                }
            }
        }
    }

    private fun stopSetup(kioskId: String) {
        kioskSetupJobMap[kioskId]?.cancel()
    }

    fun showRefreshDialog(show: Boolean) {
        setState {
            copy(showDialog = show)
        }
    }

    fun showAboutDialog(show: Boolean) {
        setState {
            copy(menuState = menuState.copy(showAboutDialog = show))
        }
    }

    fun selectKiosk(kioskId: String) {
        setState {
            copy(selectedKioskId = kioskId)
        }
    }

    fun getSelectedDeviceTitle(kioskId: String?) =
        if (kioskId != null) uiState.kioskDevices.getKioskStateById(kioskId).getTitle()
        else "N/A"

    fun getSelectedDeviceLog(kioskId: String?) = if (kioskId != null) {
        uiState.kioskDevices.getKioskStateById(kioskId).logs
    } else listOf("Please choose device to view log!")

    private fun updateItemProgress(deviceId: String, taskIndex: Int) {
        setState {
            updateItem(deviceId) {
                it.copy(
                    completedTasks = taskIndex
                )
            }
        }
    }

    private fun updateItemLog(deviceId: String, log: String) {
        FileLogger.log(deviceId, log)
        setState {
            updateItem(deviceId) {
                it.copy(
                    logs = it.logs.toMutableList().apply {
                        add(buildString {
                            append(FileLogger.logTimestampFormat.format(System.currentTimeMillis()))
                                .append(log.trimEnd())
                        })
                    }
                )
            }
        }
    }

    private fun SetupUiState.updateItem(
        deviceId: String, transformer: (AndroidDeviceUiState) -> AndroidDeviceUiState
    ): SetupUiState = copy(kioskDevices = kioskDevices.updateItem(deviceId, transformer))

    private fun List<AndroidDeviceUiState>.updateItem(
        deviceId: String, transformer: (AndroidDeviceUiState) -> AndroidDeviceUiState
    ): List<AndroidDeviceUiState> = map { if (deviceId == it.getId()) transformer(it) else it }

    private fun List<AndroidDeviceUiState>.getKioskStateById(deviceId: String) = first { it.getId() == deviceId }

    private inline fun setState(update: SetupUiState.() -> SetupUiState) {
        uiState = uiState.update()
    }

    fun onDeviceChecked(kioskId: String, checked: Boolean) {
        setState {
            updateItem(kioskId) {
                it.copy(checked = checked)
            }
        }
    }

    fun toggleBatchMode() {
        setState {
            copy(isBatchMode = !isBatchMode)
        }
    }

    fun selectAll() {
        setState {
            copy(kioskDevices = kioskDevices.map { it.copy(checked = true) })
        }
    }

    fun unselectAll() {
        setState {
            copy(kioskDevices = kioskDevices.map { it.copy(checked = false) })
        }
    }

    suspend fun startBatchSetup() {
        val allSelectedDevices = uiState.kioskDevices.filter { it.checked && it.setupState != KioskSetupState.RUNNING }
        if (allSelectedDevices.isEmpty()) {
            uiState.snackBarState.showSnackbar("Please select at least 1 valid kiosk to start setup!")
            return
        }
        allSelectedDevices.forEach {
            toggleSetupState(it.getId(), it.setupState)
        }
    }

    suspend fun stopBatchSetup() {
        val allSelectedDevices =
            uiState.kioskDevices.filter {
                it.checked && it.setupState in listOf(
                    KioskSetupState.RUNNING,
                    KioskSetupState.ERROR
                )
            }
        if (allSelectedDevices.isEmpty()) {
            uiState.snackBarState.showSnackbar("Please select at least 1 valid kiosk to stop setup!")
            return
        }
        allSelectedDevices.forEach {
            stopSetup(it.getId())
        }
    }
}