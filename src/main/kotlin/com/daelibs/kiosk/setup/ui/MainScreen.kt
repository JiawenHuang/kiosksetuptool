package com.daelibs.kiosk.setup.ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun main(args: Array<String>) {
    val appVersion = args[0].split("=")[1]
    application {
        Window(
            onCloseRequest = ::exitApplication,
            title = "Daelibs Setup Tool",
            state = rememberWindowState(
                position = WindowPosition(alignment = Alignment.Center),
            ),
        ) {
            val viewModel = remember { MainViewModel() }
            val scope = rememberCoroutineScope()
            MenuBar {
                Menu("Devices") {
                    Item("Select all", onClick = { viewModel.selectAll() }, enabled = viewModel.uiState.isBatchMode)
                    Item("Unselect all", onClick = { viewModel.unselectAll() }, enabled = viewModel.uiState.isBatchMode)
                    Separator()
                    Item(
                        "Start batch setup",
                        onClick = { scope.launch { viewModel.startBatchSetup() } },
                        enabled = viewModel.uiState.isBatchMode
                    )
                    Item(
                        "Stop batch setup",
                        onClick = { scope.launch { viewModel.stopBatchSetup() } },
                        enabled = viewModel.uiState.isBatchMode
                    )
                }
                Menu("About") {
                    Item("About", onClick = { viewModel.showAboutDialog(true) })
                }

            }
            MaterialTheme {
                MainScreen(
                    appVersion = appVersion,
                    viewModel = viewModel,
                    scope = scope,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }
}

@Composable
fun MainScreen(appVersion: String, viewModel: MainViewModel, scope: CoroutineScope, modifier: Modifier = Modifier) {
    val uiState = viewModel.uiState
    Box(contentAlignment = Alignment.BottomCenter) {
        SetupScreen(
            modifier = modifier,
            isBatchMode = uiState.isBatchMode,
            selectedTitle = viewModel.getSelectedDeviceTitle(uiState.selectedKioskId),
            selectedDeviceLogs = viewModel.getSelectedDeviceLog(uiState.selectedKioskId),
            deviceList = uiState.kioskDevices,
            toggleBatchMode = viewModel::toggleBatchMode,
            onRefreshList = { viewModel.showRefreshDialog(true) },
            onDeviceChecked = viewModel::onDeviceChecked,
            onDeviceSelected = viewModel::selectKiosk,
            onSetupStateChange = viewModel::toggleSetupState
        )
        SnackbarHost(hostState = uiState.snackBarState)
    }
    LaunchedEffect(true) {
        viewModel.refresh()
    }
    if (uiState.showDialog) {
        ConfirmDialog(
            onConfirm = {
                scope.launch {
                    viewModel.refresh()
                }
            },
            onDismiss = { viewModel.showRefreshDialog(false) })
    }
    if (uiState.menuState.showAboutDialog) {
        AboutDialog(appVersion, onDismiss = { viewModel.showAboutDialog(false) })
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun ConfirmDialog(onDismiss: () -> Unit, onConfirm: () -> Unit) {
    AlertDialog(
        modifier = Modifier.fillMaxWidth(0.7f),
        onDismissRequest = onDismiss,
        confirmButton = {
            TextButton(onClick = onConfirm)
            { Text(text = "OK") }
        },
        dismissButton = {
            TextButton(onClick = onDismiss)
            { Text(text = "Cancel") }
        },
        title = { Text(text = "Refresh connected kiosk list") },
        text = { Text(text = "This will cancel all the current existing setup processes, continue?") }
    )
}

@Composable
private fun AboutDialog(appVersion: String, onDismiss: () -> Unit) {
    Dialog(onCloseRequest = onDismiss, title = "About Daelibs Setup Tool", resizable = false) {
        Surface(shape = RoundedCornerShape(16.dp)) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(text = "Version: $appVersion", style = MaterialTheme.typography.h5)
                }
            }
        }
    }
}